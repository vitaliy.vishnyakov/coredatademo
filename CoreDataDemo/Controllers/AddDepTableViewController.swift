//
//  AddDepTableViewController.swift
//  CoreDataDemo
//
//  Created by vitalii on 05.12.2022.
//

import UIKit
import CoreData

class AddDepTableViewController: UITableViewController {
    
    private struct Constants {
        
        static let entity = "Department"
        static let sortName = "nameDep"
        static let cellname = "CellDep"
        static let segueId = "SegueDep"
    }
    
    private var fetchController: NSFetchedResultsController = CoreDataManager.shared.fetchController(
        sortKey: Constants.sortName,
        entityName: Constants.entity
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchController.delegate = self
        
        do {
            try fetchController.performFetch()
        } catch {
            print(error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueId {
            let controller = segue.destination as! AddDepViewController
            controller.department = sender as? Department
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellname, for: indexPath)
        let department = fetchController.object(at: indexPath) as! Department
        
        cell.textLabel?.text = department.nameDep
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchController.sections?.count ?? .zero
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchController.sections {
            return sections[section].numberOfObjects
        }
        
        return .zero
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let department = fetchController.object(at: indexPath) as! Department
        performSegue(withIdentifier: Constants.segueId, sender: department)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let department = fetchController.object(at: indexPath) as! Department
            CoreDataManager.shared.context.delete(department)
            CoreDataManager.shared.saveContext()
        }
    }
}

extension AddDepTableViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?
    ) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                let department = fetchController.object(at: indexPath) as! Department
                let cell = tableView.cellForRow(at: indexPath)
                cell?.textLabel?.text = department.nameDep
            }
        @unknown default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
