//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by vitalii on 05.12.2022.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    
    private struct Constants {
        
        static let entity = "Person"
        static let sortName = "firstName" // name
        static let cellname = "Cell"
        static let segueId = "Add"
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    var fetchController: NSFetchedResultsController = CoreDataManager.shared.fetchController(
        sortKey: Constants.sortName,
        entityName: Constants.entity
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchController.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        do {
            try fetchController.performFetch()
        } catch {
            print(error)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueId {
            let controller = segue.destination as! AddPersonViewController
            controller.person = sender as? Person
        }
    }
}
  
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellname, for: indexPath)
        let person = fetchController.object(at: indexPath) as! Person
        
        cell.textLabel?.text = person.firstName
        cell.detailTextLabel?.text = String(person.age)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchController.sections?.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchController.sections {
            return sections[section].numberOfObjects
        }
        
        return .zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let person = fetchController.object(at: indexPath) as! Person
        performSegue(withIdentifier: Constants.segueId, sender: person)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let person = fetchController.object(at: indexPath) as! Person
            CoreDataManager.shared.context.delete(person)
            CoreDataManager.shared.saveContext()
        }
    }
}

extension MainViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?
    ) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
            controller.fetchedObjects?.forEach { print(($0 as! Person).comment?.comments) }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                let person = fetchController.object(at: indexPath) as! Person
                let cell = tableView.cellForRow(at: indexPath)
                cell?.textLabel?.text = person.firstName
                cell?.detailTextLabel?.text = String(person.age)
            }
        @unknown default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
