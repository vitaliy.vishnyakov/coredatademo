//
//  AddViewController.swift
//  CoreDataDemo
//
//  Created by vitalii on 05.12.2022.
//

import UIKit

class AddPersonViewController: UIViewController {
    
    private enum Constants {
        
        static let entity = "Department"
        static let sortName = "nameDep"
    }
    
    var person: Person?
    
    private var fetchController = CoreDataManager.shared.fetchController(
        sortKey: Constants.sortName,
        entityName: Constants.entity
    )
    
    @IBOutlet private weak var commentTextField: UITextField!
    @IBOutlet private weak var ageTextField: UITextField!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var depTextField: UITextField! {
        didSet {
            depTextField.inputView = UIView(frame: .zero)
            depTextField.addTarget(self, action: #selector(depTextFieldEditingBegin), for: .editingDidBegin)
        }
    }
    
    private var textDep: String? {
        didSet {
            depTextField.text = textDep
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let person = person {
            nameTextField.text = person.firstName
            ageTextField.text = String(person.age)
            depTextField.text = person.departament
            commentTextField.text = person.comment?.comments
        }
        
        do {
            try fetchController.performFetch()
        } catch {
            print(error)
        }
    }
    
    @IBAction private func cancelAddTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction private func saveAddTapped(_ sender: UIBarButtonItem) {
        if savePerson(){
            dismiss(animated: true)
        }
    }
    
    private func savePerson() -> Bool {
        if nameTextField.text!.isEmpty || ageTextField.text!.isEmpty {
            print("empty fields")
            return false
        }
        
        if person == nil {
            person = Person(
                entity: CoreDataManager.shared.entityForName(entityName: "Person"),
                insertInto: CoreDataManager.shared.context
            )
        }
        
        if
            let person,
            let age = ageTextField.text,
            let name = nameTextField.text,
            let dep = depTextField.text,
            let com = commentTextField.text
        {
            person.age = Int16(age) ?? .zero
            person.firstName = name
            person.departament = dep
            
            let comment = Comments(
                entity: CoreDataManager.shared.entityForName(entityName: "Comments"),
                insertInto: CoreDataManager.shared.context
            )
            comment.comments = com
            
            person.comment = comment
            
            CoreDataManager.shared.saveContext()
        }
        
        return true
    }
    
    @objc private func depTextFieldEditingBegin() {
        let alert = UIAlertController(title: "Take dep", message: nil, preferredStyle: .actionSheet)
        let numberOfDep = fetchController.sections![.zero].numberOfObjects
        
        if numberOfDep != .zero {
            for item in 0...numberOfDep - 1 {
                let dep = fetchController.object(at: [0, item]) as! Department
                
                alert.addAction(UIAlertAction(title: dep.nameDep, style: .default) { [weak self] _ in
                    self?.textDep = dep.nameDep ?? ""
                    self?.depTextField.resignFirstResponder()
                })
            }
        } else {
            alert.addAction(UIAlertAction(title: "?", style: .default) { [weak self] _ in
                self?.textDep = ""
                self?.depTextField.resignFirstResponder()
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { [weak self] _ in
            self?.depTextField.resignFirstResponder()
        })
        
        present(alert, animated: true)
    }
}
