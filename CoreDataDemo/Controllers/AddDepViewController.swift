//
//  AddDepViewController.swift
//  CoreDataDemo
//
//  Created by vitalii on 05.12.2022.
//

import UIKit
import CoreData

class AddDepViewController: UIViewController {
    
    var department: Department?
    
    @IBOutlet private weak var depTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let department = department {
            depTextField.text = department.nameDep
        }
    }
    
    @IBAction private func cancelAddTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction private func saveAddTapped(_ sender: UIBarButtonItem) {
        if saveDepartment() {
            dismiss(animated: true)
        }
    }
    
    
    private func saveDepartment() -> Bool {
        if depTextField.text!.isEmpty {
            print("empty fields")
            return false
        }
        
        if department == nil {
            department = Department(
                entity: CoreDataManager.shared.entityForName(entityName: "Department"),
                insertInto: CoreDataManager.shared.context
            )
        }
        
        if let department, let nameDep = depTextField.text {
            department.nameDep = nameDep
            
            CoreDataManager.shared.saveContext()
        } else {
            return false
        }
        
        return true
    }
    
}
